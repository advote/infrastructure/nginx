# Project that provides a private registry for the standard nginx image and to manage its versions. [![build status](https://gitlab.com/chvote-2.0/infra/nginx/badges/master/build.svg)](https://gitlab.com/chvote-2.0/infra/nginx/commits/master)

## Content
* Build pipeline to tag and push the latest nginx image to the gitlab private registry. 
